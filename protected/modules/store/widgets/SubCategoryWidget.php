<?php

Yii::import('application.modules.store.models.StoreCategory');

/**
 * Class CategoryWidget
 *
 * <pre>
 * <?php
 *    $this->widget('application.modules.store.widgets.CategoryWidget');
 * ?>
 * </pre>
 */
class SubCategoryWidget extends yupe\widgets\YWidget
{
    public $parent = 0;

    public $depth = 1;

    /** @var $category StoreCategory */
    public $category = null;

    public $view = 'sub-category-widget';

    public $htmlOptions = [];

    public function run()
    {
        if ($this->category) {
            $subCats = StoreCategory::model()->findAll('parent_id = ' . $this->category->id);
            $this->render($this->view, ['model' => $subCats]);
        }
    }
}
