# README #
Yupe CMS store subcategory widget

# INSTALL #
Скопировать в корневую директорию сайта.
Вывести виджет в \themes\shop\views\store\category\view.php

````
    <?php
        $this->widget('application.modules.store.widgets.SubCategoryWidget', ['category' => $category]);
    ?>
````